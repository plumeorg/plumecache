package org.plumecache.springboot.autoconfigurer;

import com.plumecache.core.InstanceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@ConfigurationProperties(prefix = "plumecache")
public class PlumecacheProperties {

    private List<InstanceProperties> instances;

    public List<InstanceProperties> getInstances() {
        return instances;
    }

    public void setInstances(List<InstanceProperties> instances) {
        this.instances = instances;
    }

}