package com.plumecache.core;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InstanceProperties {
    private String name;

    private String type;

    private String endpoint;

    private String prefix;

    private String serializer;

    private String compressor;

    private List<String> excludeInterceptors;

    private String username;

    private String password;

    private String timeout;

    private String connectTimeout;

    private String connectionPoolSize;
}