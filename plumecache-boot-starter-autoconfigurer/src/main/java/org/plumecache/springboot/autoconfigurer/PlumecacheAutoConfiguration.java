package org.plumecache.springboot.autoconfigurer;

import com.plumecache.core.CacheService;
import com.plumecache.core.CacheServiceFactory;
import com.plumecache.core.StatisticsService;
import com.plumecache.core.interceptor.CacheInterceptorProxyInvocationHandler;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.HashSet;

@Configuration
@ComponentScan("org.plumecache")
@EnableConfigurationProperties({PlumecacheProperties.class})
public class PlumecacheAutoConfiguration implements InitializingBean {

    @Autowired
    PlumecacheProperties plumecacheProperties;

    @Bean
    public CacheService cacheService() {
        return CacheServiceFactory.getInstance();
    }

    @Override
    public void afterPropertiesSet() {
        printBanner();
        CacheServiceFactory.initialize(plumecacheProperties.getInstances());
    }

    @Bean
    public StatisticsService statisticsService() {
        return new StatisticsService();
    }

    private void printBanner() {
        String s = " ____  _     _     _      _____ ____ ____  ____ _     _____\n" +
                "/  __\\/ \\   / \\ /\\/ \\__/|/  __//   _Y  _ \\/   _Y \\ /|/  __/\n" +
                "|  \\/|| |   | | ||| |\\/|||  \\  |  / | / \\||  / | |_|||  \\  \n" +
                "|  __/| |_/\\| \\_/|| |  |||  /_ |  \\_| |-|||  \\_| | |||  /_ \n" +
                "\\_/   \\____/\\____/\\_/  \\|\\____\\\\____|_/ \\|\\____|_/ \\|\\____\\";
        System.out.println(s);
    }
}
