package com.plumecache.core.interceptor;

import com.plumecache.core.CacheService;
import com.plumecache.core.annotation.Key;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Map;

public class PrefixCacheInterceptor extends BaseCacheInterceptor {
    @Override
    public Integer getOrder() {
        return Integer.MIN_VALUE;
    }

    @Override
    public boolean preHandle(CacheService target, Method method, Object[] args, Map<String, Object> context) {
        String prefix = target.getProperties().getPrefix();

        if (StringUtils.isBlank(prefix)) {
            return true;
        }

        String newKey = prefix.trim().concat("@").concat(getKey(method, args));
        setKey(method, args, newKey);
        return true;
    }


}
