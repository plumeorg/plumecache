package com.plumecache.core.serializer;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public interface ObjectCacheSerializer {

    void serialize(ObjectOutputStream objectOutputStream);

    void deserialize(ObjectInputStream objectInputStream);
}
