package com.plumecache.core.common;


import com.alibaba.ttl.TransmittableThreadLocal;

import java.util.HashMap;
import java.util.Map;

public class CacheContext {
    private static final ThreadLocal<Map<String, String>> THREAD_LOCAL = new TransmittableThreadLocal<>();

    public static void set(String key, String value) {

        if (null == THREAD_LOCAL.get()) {
            THREAD_LOCAL.set(new HashMap<>(16));
        }
        THREAD_LOCAL.get().put(key, value);
    }

    public static String get(String key) {
        if (null == THREAD_LOCAL.get()) {
            return null;
        }
        
        return THREAD_LOCAL.get().get(key);
    }

    public static void clear() {
        THREAD_LOCAL.remove();
    }
}
