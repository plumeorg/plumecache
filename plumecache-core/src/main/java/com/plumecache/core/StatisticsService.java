package com.plumecache.core;

import com.plumecache.core.common.StatisticsDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class StatisticsService {
    public List<StatisticsDTO> getStatisticsDTOList() {
        Map<String, CacheService> cacheServiceMap = CacheServiceFactory.getCacheServiceMap();

        List<StatisticsDTO> statisticsDTOList = new ArrayList<>();
        cacheServiceMap.forEach((instanceName, cacheService) -> {
            statisticsDTOList.add(getStatisticsDTO(cacheService));
        });

        return statisticsDTOList;
    }

    private StatisticsDTO getStatisticsDTO(CacheService cacheService) {
        StatisticsDTO statisticsDTO = new StatisticsDTO();
        statisticsDTO.setProperties(cacheService.getProperties());
        statisticsDTO.setStatistics(cacheService.getStatistics());
        return statisticsDTO;
    }
}
