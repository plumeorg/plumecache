package com.plumecache.core.interceptor;

import com.plumecache.core.CacheService;

import java.lang.reflect.Method;
import java.util.Map;

public interface CacheInterceptor {
    default Integer getOrder() {
        return 0;
    }

    default boolean isEnable() {
        return true;
    }

    default boolean preHandle(CacheService target, Method method, Object[] args, Map<String, Object> context) {
        return true;
    }

    default void postHandle(CacheService target, Method method, Object[] args, Map<String, Object> context, Object result) {
    }

    default void afterCompletion(CacheService target, Method method, Object[] args, Map<String, Object> context, Exception ex) {
    }
}
