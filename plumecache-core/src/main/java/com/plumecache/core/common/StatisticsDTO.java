package com.plumecache.core.common;

import com.plumecache.core.InstanceProperties;
import lombok.Data;

import java.util.Map;

@Data
public class StatisticsDTO {
    private InstanceProperties properties;
    private Map<String, String> statistics;
}
