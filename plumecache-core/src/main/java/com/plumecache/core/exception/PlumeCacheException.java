package com.plumecache.core.exception;

public class PlumeCacheException extends RuntimeException {

    public PlumeCacheException(String message) {
        super(message);
    }

    public PlumeCacheException(Throwable throwable) {
        super(throwable);
    }
}
