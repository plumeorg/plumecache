package org.plumecache.samples.test;

import com.plumecache.core.*;
import com.plumecache.core.common.StatisticsDTO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.plumecache.samples.ApiApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApiApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SpringTest {

    @Test
    public void testRedisCluster() {
        InstanceProperties properties = new InstanceProperties();
        properties.setEndpoint("10.100.62.26:6605,10.100.62.27:6605,10.100.62.28:6605");

        BaseCacheService plumeCacheService = new ClusterRedisCacheService();
        plumeCacheService.initialize(properties);

        plumeCacheService.set("name", "anson.yin");
        Assert.assertEquals("anson.yin", plumeCacheService.get("name", String.class));
        System.out.println(plumeCacheService.getStatistics());
    }

    @Test
    public void testRedisSingle() {
        InstanceProperties properties = new InstanceProperties();
        properties.setEndpoint("127.0.0.1:6379");

        BaseCacheService plumeCacheService = new SingleRedisCacheService();
        plumeCacheService.initialize(properties);

        plumeCacheService.set("name", "anson.yin");
        Assert.assertEquals("anson.yin", plumeCacheService.get("name", String.class));

        System.out.println(plumeCacheService.getStatistics());
    }

    @Test
    public void testMemcached() {
        InstanceProperties properties = new InstanceProperties();
        properties.setEndpoint("127.0.0.1:11211");

        BaseCacheService plumeCacheService = new MemcachedCacheService();
        plumeCacheService.initialize(properties);

        plumeCacheService.set("name", "anson.yin");
        Assert.assertEquals("anson.yin", plumeCacheService.get("name", String.class));
        System.out.println(plumeCacheService.getStatistics());
    }

//    @Test
//    public void testCacheFactory() {
//        List<InstanceProperties> instanceProperties = Arrays.asList(
//                InstanceProperties.builder().type("redis").endpoint("127.0.0.1:6379").prefix("order:").build()
////                InstanceProperties.builder().name("redisCluster").type("redisCluster").endpoint("10.100.62.26:6605,10.100.62.27:6605,10.100.62.28:6605").build(),
////                InstanceProperties.builder().name("memcached").type("memcached").endpoint("127.0.0.1:11211").build()
//        );
//
//        CacheServiceFactory.initialize(instanceProperties);
//
//        CacheService instance = CacheServiceFactory.getInstance();
//        instance.set("name", "memcached");
//        System.out.println(instance.get("name", String.class));
//
//    }

    @Autowired
    StatisticsService statisticsService;

    @Autowired
    CacheService cacheService;

    @Test
    public void testStatisticsService() {
        System.out.println(cacheService.incr("key1"));
        System.out.println(cacheService.incrBy("key1", 10));
        // List<StatisticsDTO> statisticsDTOList = statisticsService.getStatisticsDTOList();
    }


    @Test
    public void testMultipleInstances() {
        //和CacheService redis =CacheServiceFactory.getInstance("redis")效果一样
        CacheService redis = CacheServiceFactory.getInstance();
        redis.set("instance", "redis");
        System.out.println(redis.get("instance"));

        CacheService memcached = CacheServiceFactory.getInstance("memcached");
        memcached.set("instance", "memcached");
        System.out.println(memcached.get("instance"));
    }

}
