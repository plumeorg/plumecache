package com.plumecache.core.compressor;

public interface CacheCompressor {
    default String compress(String value) {
        return value;
    }

    default String decompress(String value) {
        return value;
    }
}
