package org.plumecache.samples;

import com.plumecache.core.annotation.CacheCompress;
import com.plumecache.core.serializer.ObjectCacheSerializer;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.ToString;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Date;

@Data
@CacheCompress
@ToString
public class Student implements ObjectCacheSerializer {
    private String name;
    private Integer age;
    private String address;
    private Date birthday;

    @SneakyThrows
    @Override
    public void serialize(ObjectOutputStream objectOutputStream) {
        objectOutputStream.writeObject(name);
        objectOutputStream.writeObject(age);
    }

    @SneakyThrows
    @Override
    public void deserialize(ObjectInputStream objectInputStream) {
        this.setName(String.valueOf(objectInputStream.readObject()));
        this.setAge((Integer) objectInputStream.readObject());
    }
}
