package com.plumecache.core.serializer;

public interface CacheSerializer {
    <T> String serialize(T value);

    <T> T deserialize(String value, Class<T> clazz);
}
