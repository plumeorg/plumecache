package org.plumecache.samples.controller;

import com.plumecache.core.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping
@RestController
public class SampleApiController {

    @Autowired
    private CacheService cacheService;

    @RequestMapping(value = "/hello")
    public String hello() {
        cacheService.set("name", "anson.yin");
        return "hello,".concat(cacheService.get("name"));
    }
}
