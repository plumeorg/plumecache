const IS_PROD = ['production'].includes(process.env.NODE_ENV)
module.exports = {
    lintOnSave: false,
    productionSourceMap: !IS_PROD,
    publicPath: './',
    devServer: {
       historyApiFallback: true,

        disableHostCheck: true,
        https:false,
        port:10000,
        proxy: {
            '/plumecache/*': {
                target: ' http://localhost:8080',
                changeOrigin: true,
                secure: false,
                logLevel: "debug",
                pathRewrite: { "^/plumecache": "/plumecache" }
            },

        }
    }
}
