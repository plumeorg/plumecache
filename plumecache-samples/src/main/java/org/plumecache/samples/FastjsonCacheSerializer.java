package org.plumecache.samples;

import com.alibaba.fastjson.JSON;
import com.plumecache.core.serializer.CacheSerializer;

public class FastjsonCacheSerializer implements CacheSerializer {

    @Override
    public <T> String serialize(T value) {
        return JSON.toJSONString(value);
    }

    @Override
    public <T> T deserialize(String value, Class<T> clazz) {
        return JSON.parseObject(value, clazz);
    }
}
