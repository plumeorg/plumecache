package org.plumecache.samples.test;

import com.alibaba.fastjson.JSON;
import com.google.gson.Gson;
import com.plumecache.core.*;
import com.plumecache.core.common.CacheContext;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.plumecache.samples.ApiApplication;
import org.plumecache.samples.Student;
import org.plumecache.samples.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Date;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApiApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CoreTest {

    @Test
    public void shouldAnswerWithTrue() {
        InstanceProperties properties = new InstanceProperties();
        properties.setEndpoint("127.0.0.1:6379");

        BaseCacheService plumeCacheService = new SingleRedisCacheService();
        plumeCacheService.initialize(properties);


        plumeCacheService.set("name", "anson.yin");
        Assert.assertEquals("anson.yin", plumeCacheService.get("name", String.class));
    }

    @Autowired
    private CacheService cacheService;

    @Test
    public void shouldAnswerWithTrue2() {
        cacheService.set("age", "18");
        System.out.println(cacheService.get("age"));
    }

    @Test
    public void testCacheFilter() {
//        cacheService.set("name", "123123123");
        String name = cacheService.get("name11");
        System.out.println(name);
    }

    Gson gson = new Gson();

    @Test
    public void testObject() {
        User user = new User();
        user.setAge(100);
        user.setName("zhangsan");

//        System.out.printf(gson.toJson(user));

        cacheService.set("user", user);
        User cacheUser = cacheService.get("user", User.class);
        System.out.println(cacheUser);
    }


    @Test
    public void testLock() throws InvocationTargetException, IllegalAccessException {
//        System.out.println(cacheService.lock("key", 100));
//
//        System.out.println(cacheService.lock("key", 100));

        Method[] methods = cacheService.getClass().getMethods();
        Method get = Arrays.stream(methods).filter(r -> r.getName().equals("set") && r.getParameters().length == 2).findFirst().get();

        Object name = get.invoke(cacheService, "name", "aa");

        System.out.println(JSON.toJSONString(name));
    }


    @Test
    public void test() {
        Student student = new Student();
        student.setName("anson.yin");
        student.setBirthday(new Date());
        student.setAddress("suzhou");
        student.setAge(18);

        CacheService cacheService = CacheServiceFactory.getInstance();
//        cacheService.set("name", "anson.yin");
//        System.out.println(cacheService.get("name"));

        cacheService.set("student", student);

        System.out.println(cacheService.get("student", Student.class));
    }

    @Test
    public void testCompress() {
        User user = new User();
        user.setAge(100);
        user.setName("zhangsanfeng");
        user.setAddress("zhangsanfengzhangsanfengzhangsanfengzhangsanfengzhangsanfengzhangsanfengzhangsanfengzhangsanfengzhangsanfengzhangsanfengzhangsanfeng");

        cacheService.set("user", user);
        User cacheUser = cacheService.get("user", User.class);

        System.out.println(cacheUser);
    }

    @Test
    public void testVersion() {
//        User user = new User();
//        user.setAge(100);
//        user.setName("zhangsanfeng");
//        user.setAddress("address");
//
//        cacheService.set("user", user);
//        User cacheUser = cacheService.get("user", User.class);
//
//        System.out.println(cacheUser);

        CacheContext.set("a", "1123");


        f();
    }

    private void f() {
        System.out.println("1" + CacheContext.get("a"));

        new Thread(() -> {
            System.out.println("2" + CacheContext.get("a"));
        }).start();

        System.out.println("3" + CacheContext.get("a"));

        CacheContext.clear();
        System.out.println("4" + CacheContext.get("a"));
    }
}
