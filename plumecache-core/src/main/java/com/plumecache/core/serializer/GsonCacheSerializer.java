package com.plumecache.core.serializer;

import com.google.gson.Gson;

public class GsonCacheSerializer implements CacheSerializer {
    private static final Gson GSON = new Gson();

    @Override
    public <T> String serialize(T value) {
        return GSON.toJson(value);
    }

    @Override
    public <T> T deserialize(String value, Class<T> clazz) {
        return GSON.fromJson(value, clazz);
    }
}
