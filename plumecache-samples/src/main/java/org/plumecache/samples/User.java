package org.plumecache.samples;

import com.plumecache.core.annotation.CacheVersion;
import lombok.Data;

@Data
@CacheVersion("2.0")
public class User {
    private String name;
    private Integer age;
    private String address;
}
