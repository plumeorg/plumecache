package com.plumecache.core.interceptor;

import com.plumecache.core.CacheService;

import java.lang.reflect.Proxy;

public class CacheServiceProxy {
    public static CacheService newProxyInstance(CacheService target) {
        return (CacheService) Proxy.newProxyInstance(target.getClass().getClassLoader(),
                new Class<?>[]{CacheService.class}, new CacheInterceptorProxyInvocationHandler(target));
    }
}
