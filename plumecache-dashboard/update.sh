echo '--start build--'
npm run build

echo '-delete resources file-'
rm -rf ../plumecache-boot-starter-autoconfigurer/src/main/resources/templates/*
rm -rf ../plumecache-boot-starter-autoconfigurer/src/main/resources/META-INF/resources/plumecache/*

echo '--copy file to ui resource--'

cp ./dist/index.html ../plumecache-boot-starter-autoconfigurer/src/main/resources/templates/plumecache.html
cp -R ./dist/. ../plumecache-boot-starter-autoconfigurer/src/main/resources//META-INF/resources/plumecache
