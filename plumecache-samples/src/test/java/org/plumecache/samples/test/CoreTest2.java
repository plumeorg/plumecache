package org.plumecache.samples.test;

import org.junit.Test;
import sun.misc.BASE64Decoder;

import java.io.*;


public class CoreTest2 {

    @Test
    public void test2() throws IOException, ClassNotFoundException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(baos);

        objectOutputStream.write(1);
        objectOutputStream.writeObject("anson");
        objectOutputStream.flush();

        String s = new sun.misc.BASE64Encoder().encode(baos.toByteArray());
        System.out.println(s);

        byte[] bytes = new BASE64Decoder().decodeBuffer(s);

        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);

        System.out.println(objectInputStream.read());
        System.out.println(objectInputStream.readObject());
    }
}
