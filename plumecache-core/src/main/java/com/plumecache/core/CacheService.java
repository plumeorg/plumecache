package com.plumecache.core;

import com.plumecache.core.annotation.*;
import net.rubyeye.xmemcached.MemcachedClient;
import org.redisson.api.RedissonClient;

import java.util.Map;

public interface CacheService {
    @IgnoreInterceptor
    void initialize(InstanceProperties properties);

    @IgnoreInterceptor
    InstanceProperties getProperties();

    @IgnoreInterceptor
    Map<String, String> getStatistics();

    @IgnoreInterceptor
    default RedissonClient getRedissonClient() {
        return null;
    }

    @IgnoreInterceptor
    default MemcachedClient getMemcachedClient() {
        return null;
    }

    boolean exists(@Key String key);

    <T> void set(@Key String key, @Value T value);

    <T> void set(@Key String key, @Value T value, Integer ttl);

    <T> T get(@Key String key, @ValueClazz Class<T> clazz);

    String get(@Key String key);

    void delete(@Key String key);

    void expire(@Key String key, Integer ttl);

    Long ttl(@Key String key);

    long incr(@Key String key);

    long incrBy(@Key String key, Integer delta);

    <T> T hget(@Key String key, @Field String field, @ValueClazz Class<T> clazz);

    String hget(@Key String key, @Field String field);

    <T> void hset(@Key String key, @Field String field, @ValueClazz T value);

    long hdel(@Key String key, @Field String field);

    <T> Map<String, T> hgetAll(@Key String key, @ValueClazz Class<T> clazz);

    Map<String, String> hgetAll(@Key String key);

    boolean lock(@Key String key, Integer ttl);

    void release(@Key String key);
}
