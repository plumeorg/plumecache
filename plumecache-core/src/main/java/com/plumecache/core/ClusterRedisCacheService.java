package com.plumecache.core;

import com.plumecache.core.exception.PlumeCacheException;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.redisson.Redisson;
import org.redisson.api.redisnode.RedisCluster;
import org.redisson.api.redisnode.RedisNode;
import org.redisson.api.redisnode.RedisNodes;
import org.redisson.config.ClusterServersConfig;
import org.redisson.config.Config;

import java.util.Arrays;
import java.util.Map;

public class ClusterRedisCacheService extends RedisCacheService {

    @Override
    public void initialize(InstanceProperties properties) {
        super.initialize(properties);

        String endpoint = properties.getEndpoint();
        if (StringUtils.isEmpty(endpoint)) {
            throw new PlumeCacheException("can't find [endpoint] properties");
        }

        String[] nodeAddressArray = Arrays.stream(endpoint.split(",")).map(r -> "redis://" + r).toArray(String[]::new);
        Config config = new Config();

        ClusterServersConfig clusterServersConfig = config.useClusterServers()
                .addNodeAddress(nodeAddressArray);

        if (StringUtils.isNotBlank(properties.getUsername())) {
            clusterServersConfig.setUsername(properties.getUsername());
        }

        if (StringUtils.isNotBlank(properties.getPassword())) {
            clusterServersConfig.setPassword(properties.getPassword());
        }

        if (StringUtils.isNotBlank(properties.getTimeout())) {
            clusterServersConfig.setTimeout(Integer.parseInt(properties.getTimeout()));
        }

        if (StringUtils.isNotBlank(properties.getConnectTimeout())) {
            clusterServersConfig.setConnectTimeout(Integer.parseInt(properties.getConnectTimeout()));
        }

        if (StringUtils.isNotBlank(properties.getConnectionPoolSize())) {
            clusterServersConfig.setMasterConnectionPoolSize(Integer.parseInt(properties.getConnectionPoolSize()));
            clusterServersConfig.setSlaveConnectionPoolSize(Integer.parseInt(properties.getConnectionPoolSize()));
        }

        redissonClient = Redisson.create(config);
    }

    @SneakyThrows
    @Override
    public Map<String, String> getStatistics() {
        RedisCluster redisNodes = redissonClient.getRedisNodes(RedisNodes.CLUSTER);
        return redisNodes.getMasters().stream().findFirst().get().info(RedisNode.InfoSection.ALL);
    }
}
