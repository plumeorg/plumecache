import {service} from "@/api/service";

export function listNodes() {
    return service.get("list")
}


export function getStatistics() {
    return service.get("getStatistics")
}


export function exec(data) {
    return service.post("execute", data)
}
